"""
script for reading email.
ways of reading emails:
    By default, only unread emails will be read returned. once the script runs, all unread mails will be marked as read
    if SEARCH_BY_UID is set to 1, emails will be searched starting from last saved UID.
        once emails have been read by this script, they are saved to a database together with UIDs. if SEARCH_BY_UID is
        set to 1, the last saved email's UID is used to as reference, and all UIDs greater than it will be fetched.

SEARCH_BY_UID is disabled by default and emails will be fetched if they haven't been read before
"""

import email.utils
import logging
import os
from email.mime.text import MIMEText

from imap_tools import MailBox, U, AND, OR
import smtplib

import bison
from bison import MAIL_HOST, MAIL_USER, MAIL_PASS, SMTP_PORT, SEARCH_BY_UID

LOG_FORMAT = '%(asctime)s %(levelname)s [%(name)s:%(lineno)s] %(module)s %(message)s'
logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
logger = logging.getLogger(__name__)


class PGBison:
    def __init__(self) -> None:
        self.MAIL_HOST = MAIL_HOST
        self.MAIL_USER = MAIL_USER
        self.MAIL_PASS = MAIL_PASS
        self.SMTP_PORT = SMTP_PORT
        self.SENDERS = bison.MAIL_SOURCES

    def connect(self):
        mail = MailBox("onfonmedia.com")

        try:
            mail.login(self.MAIL_USER, self.MAIL_PASS, initial_folder='INBOX')
        except Exception as e:
            logger.exception(f"ERROR: {str(e)}")
            logger.info("exiting...")
            exit(1)
        return mail
    
    def subs_login(self):
        mail = MailBox(self.MAIL_HOST)
        mail.login(self.MAIL_USER, self.MAIL_PASS, initial_folder='INBOX')
        return mail

    def get_mail(self, mail, start_uid: str):
        if len(self.SENDERS) == 0:
            logger.error("MAIL_SOURCES must be set in environment variables")
            return
        # result = []
        # for msg in mail.fetch(AND(uid=U(start_uid, '*'))):
        #     if int(msg.uid) > int(start_uid):
        #         print(F"tag: {msg.flags}")
        #         result.append((f"<mail>{msg.text}</mail>", msg.uid, msg.from_))
        if SEARCH_BY_UID:
            res = [(f"<mail>{msg.text}</mail>", msg.uid, msg.from_) for msg in mail.fetch(AND(uid=U(start_uid, '*'))) if int(msg.uid) > int(start_uid) and msg.from_ in self.SENDERS]

        else:
            res = [(f"<mail>{msg.text}</mail>", msg.uid, msg.from_) for msg in mail.fetch(AND(seen=False)) if msg.from_ in self.SENDERS]

        return res
        # return result

    def out_mail_connection(self):
        try:
            logger.info("Establishing connection to server...")
            server = smtplib.SMTP(self.MAIL_HOST, port=SMTP_PORT)
            server.set_debuglevel(1)
            server.starttls()  # Secure the connection
            logger.info("logging in..")
            server.login(self.MAIL_USER, self.MAIL_PASS)

        except Exception as e:
            # Print any error messages to stdout
            logger.exception(f"Error: {e}")
            exit(1)
            return
        return server

    def send_mail(self, message, receiver_mail, server=None):
        if server is None:
            server = self.out_mail_connection()
        else:
            server = server

        msg = MIMEText(f'{message}.')
        msg['To'] = email.utils.formataddr((f'{receiver_mail}', f'{receiver_mail}'))
        msg['From'] = email.utils.formataddr((f'{self.MAIL_USER}', f'{self.MAIL_USER}'))
        msg['Subject'] = 'SMS received.'

        try:
            logger.info("sending email...")
            resp = server.sendmail(self.MAIL_USER, [receiver_mail], msg.as_string())
        except Exception as e:
            logger.exception(str(e))
            exit(1)
            return
        return resp
