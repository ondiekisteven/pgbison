"""
for sending sms on background process.
to disable this script, set the environment variable SMS_QUEUE_MESSAGES to 0 or false

when disabled, sms will be sent immediately they are read, within main.py. has no effect if messages are few.

**THIS SCRIPT IS ENABLED BY DEFAULT**
"""

import logging
import os

from json import dumps
from time import sleep

import requests
from sqlalchemy.orm import sessionmaker

from bison.db import OutgoingSms, DATABASE, init_db
from bison import SMS_SENDER_ID, SMS_CLIENT_ID, SMS_API_KEY, SMS_ACCESS_KEY, SMS_COOKIE, SMS_URL

LOG_FORMAT = '%(asctime)s %(levelname)s [%(name)s:%(lineno)s] %(module)s %(message)s'
logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
logger = logging.getLogger(__name__)

Session = sessionmaker(DATABASE)
session = Session()


def get_unsent() -> list:
    messages = session.query(OutgoingSms).filter(OutgoingSms.sent == False)
    unsent = []

    for message in messages:
        msg = {
            'id': message.id,
            'SmsRecipient': message.receiver,
            'SmsText': message.message
        }
        unsent.append(msg)
    return unsent


def get_payload(messages=None):
    if messages is None:
        messages = get_unsent()
    else:
        messages = messages
    user_messages = []

    for sms in messages:
        user_messages.append(
            {
                "Number": sms["SmsRecipient"],
                "Text": sms["SmsText"]
            }
        )

    payload = {
        "SenderId": SMS_SENDER_ID,
        "MessageParameters": user_messages,
        "ApiKey": SMS_API_KEY,
        "ClientId": SMS_CLIENT_ID
    }
    return payload


def mark_sent(unsent_messages):
    for message in unsent_messages:
        row = session.query(OutgoingSms).get(message["id"])
        row.sent = True
    session.commit()


def send_sms():
    unsent = get_unsent()
    if len(unsent) == 0:
        return
    msg_payload = get_payload(unsent)
    payload = dumps(msg_payload)
    logger.info(f"PAYLOAD IS {payload}")
    headers = {
        'Content-Type': 'application/json',
        'AccessKey': SMS_ACCESS_KEY,
        'Cookie': SMS_COOKIE
    }

    response = requests.request("POST", SMS_URL, headers=headers, data=payload)

    logger.info(response.text.encode('utf8'))
    mark_sent(unsent)


def start():
    init_db()
    queue_messages = os.environ.get('SMS_QUEUE_MESSAGES', True)
    if not queue_messages:
        logger.warning('messages not queued. exiting..')
        exit(1)
    logger.info('Starting background sms engine...')
    while True:
        send_sms()
        sleep(5)


if __name__ == '__main__':
    start()
