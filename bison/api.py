"""
sms delivery messages api.
this flask server will receive and save delivery reports after sms messages have been sent
"""

import logging

from flask import Flask, request
from sqlalchemy.orm import sessionmaker

from bison import db

app = Flask(__name__)
Session = sessionmaker(db.DATABASE)
session = Session()


@app.route('/')
def test():
    return 'api is working'


@app.route('/sms/', methods=['POST'])
def receive_sms():
    payload = request.json

    new_message = db.DeliveryReport(
        done_date=payload['doneDate'],
        error_code=payload['errorCode'],
        message_id=payload['messageId'],
        short_message=payload['shortMessage'],
        status=payload['status'],
        submit_date=payload['submitDate'],
        mobile=payload['mobile']
    )
    session.add(new_message)
    session.commit()
    logging.info(f"SAVED MESSAGE FOR {payload['mobile']}")
    return new_message.status


def start():
    logging.info('starting sms dlr api...')
    app.run()


if __name__ == '__main__':
    start()
