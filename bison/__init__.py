import os

DB_STRING = os.environ.get('DB_STRING', "sqlite:///sqlite.db")

MAIL_HOST = os.environ.get('MAIL_HOST')
MAIL_USER = os.environ.get("MAIL_USER")
MAIL_PASS = os.environ.get("MAIL_PASS")
IMAP_PORT = os.environ.get("IMAP_PORT", "143")
SMTP_PORT = os.environ.get("SMTP_PORT", '587')

if not MAIL_USER or not MAIL_PASS:
    raise NotImplementedError("environment variables MAIL_USER and MAIL_USER must be set")

SMS_URL = "https://api.onfonmedia.co.ke/v1/sms/SendBulkSMS"
SMS_SENDER_ID = os.environ.get('SMS_SENDER_ID', "PG_BISON")
SMS_API_KEY = os.environ.get('SMS_API_KEY')
SMS_CLIENT_ID = os.environ.get('SMS_CLIENT_ID')
SMS_COOKIE = os.environ.get('SMS_COOKIE')
SMS_ACCESS_KEY = os.environ.get('SMS_ACCESS_KEY')

if not SMS_SENDER_ID or not SMS_API_KEY or not SMS_CLIENT_ID or not SMS_COOKIE or not SMS_ACCESS_KEY:
    raise NotImplementedError("SMS credentials not set properly")

# CONFIGURATION TO ALLOW QUEUING OF MESSAGES THEN SEND LATER
queue_messages = os.environ.get('SMS_QUEUE_MESSAGES', '1')
if queue_messages.lower() in ['0', 'false']:
    QUEUE_MESSAGES = False
else:
    QUEUE_MESSAGES = True


# configuration to enable or disable email replies
# if set to 0 or not set, no email will be sent back once we read an email. if set to true or 1,
_mail_delivery = os.environ.get('ENABLE_MAIL_REPLY', '')
if _mail_delivery in ['', 0]:
    ENABLE_MAIL_REPLY = False
else:
    ENABLE_MAIL_REPLY = True


# this configuration determines how the email engine searches for emails
# when set to 1 or true or equivalent truth value, UIDs are used to track which emails to read. the last saved email's
#   UID is used as reference, and new mails fetched are those greater than this UID
#
# if not set or set to false value, UIDs are not used. the engine will fetch unread emails. unread emails will be
#   be those which contain the flag SEEN set to false. this is the default setting
_search_by_uid = os.environ.get('SEARCH_BY_UID', '')
if _search_by_uid in ['', '0']:
    SEARCH_BY_UID = False
else:
    SEARCH_BY_UID = True


# this configuration enables the sms delivery reports api. default is enabled
_enable_sms_delivery_api = os.environ.get('ENABLE_SMS_DLRS_API', '')
if _enable_sms_delivery_api.lower() in ['', '1', 'yes', 'true']:
    ENABLE_SMS_DLRS = True
else:
    ENABLE_SMS_DLRS = False


# for configuring emails which the script should fetch and read. MUST BE SET
_mail_sources = os.environ.get("MAIL_SOURCES")
if _mail_sources:
    MAIL_SOURCES = _mail_sources.split(",")
else:
    MAIL_SOURCES = []
    raise NotImplementedError("environment variable 'MAIL_SOURCES' must be set. ")
