#!/bin/sh


# checking and waiting for postgres to start
if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $POSTGRES_HOST $POSTGRES_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

# for starting read_email script and background_sms script. *****THESE IS THE MOST IMPORTANT COMMAND*****
python -m $MODULE &

# for starting sms Delivery server
gunicorn --bind 0.0.0.0:5000 bison.api:app

exec "$@"
