import logging

import multiprocessing
from bison.db import init_db

from bison import QUEUE_MESSAGES, ENABLE_MAIL_REPLY, ENABLE_SMS_DLRS

LOG_FORMAT = '%(asctime)s %(levelname)s [%(name)s:%(lineno)s] %(module)s %(message)s'
logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
logger = logging.getLogger(__name__)

base_modules = [
    'bison.main',
]


def start_module(module):
    def t():
        try:
            mod = __import__(module, fromlist=['start'])
            logger.info(f"<<----- {module} IMPORTED SUCCESSFULLY----->>")
            start = getattr(mod, 'start')
            start()
        except Exception as e:
            logger.exception(f"ERROR: {str(e)}")
            exit(1)
    return t


def start_modules():
    # initialize database
    init_db()

    modules = []

    if QUEUE_MESSAGES:
        modules.append('bison.background_sms')

    if ENABLE_MAIL_REPLY:
        modules.append('bison.mail_reply')

    modules += base_modules

    tasks = []
    logger.info(f"<<----- DISCOVERED MODULES ----->>")
    logger.info(modules)
    for mod in modules:
        tasks.append(multiprocessing.Process(target=start_module(mod)))

    print(f"tasks to start: {tasks}")
    for task in tasks:
        task.start()

    for j in tasks:
        j.join()


if __name__ == '__main__':

    start_modules()
