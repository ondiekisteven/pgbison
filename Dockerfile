# pull official base image
FROM python:3.8.3


# set work directory
WORKDIR /usr/src/app

# install dependencies
RUN apt-get update && apt-get install -y netcat
COPY ./requirements.txt .
RUN pip install -r requirements.txt

# copy project
COPY . .

COPY run.sh /app
CMD [ "sh", "run.sh"]

