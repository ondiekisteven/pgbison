import logging

from sqlalchemy import create_engine
from sqlalchemy import Column, String, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.sqltypes import DateTime, Integer, Boolean

from bison import DB_STRING

LOG_FORMAT = '%(asctime)s %(levelname)s [%(name)s:%(lineno)s] %(module)s %(message)s'
logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
logger = logging.getLogger(__name__)

BASE = declarative_base()
db_string = DB_STRING
DATABASE = create_engine(db_string)


def init_db():
    logger.info("initializing database")
    BASE.metadata.create_all(DATABASE)


class MailResult(BASE):
    __tablename__ = 'mail_result'

    id = Column(Integer, primary_key=True, autoincrement=True)
    uid = Column(Integer)
    sender = Column(String)
    body = Column(Text)
    date_created = Column(DateTime(timezone=True), server_default=func.now())


class OutgoingSms(BASE):
    __tablename__ = 'sent_message'

    id = Column(Integer, primary_key=True, autoincrement=True)
    sender_id = Column(String)
    receiver = Column(String)
    message = Column(Text)
    sent_time = Column(DateTime(timezone=True), server_default=func.now())
    sent = Column(Boolean, default=False)


class DeliveryReport(BASE):

    __tablename__ = 'delivery_reports'

    id = Column(Integer, primary_key=True, autoincrement=True)
    done_date = Column(String)
    error_code = Column(String)
    message_id = Column(String)
    short_message = Column(String)
    status = Column(String)
    submit_date = Column(String)
    mobile = Column(String)
    date_created = Column(DateTime(timezone=True), server_default=func.now())
