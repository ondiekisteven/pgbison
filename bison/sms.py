import logging
from json import dumps
import requests

from bison import SMS_URL, SMS_COOKIE, SMS_CLIENT_ID, SMS_API_KEY, SMS_ACCESS_KEY, SMS_SENDER_ID
from .db import OutgoingSms

LOG_FORMAT = '%(asctime)s %(levelname)s [%(name)s:%(lineno)s] %(module)s %(message)s'
logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
logger = logging.getLogger(__name__)


class SMS:
    def __init__(self, xml_result: list) -> None:
        self.users = xml_result

    def get_payload(self):
        user_messages = []
        for user in self.users:
            user_messages.append(
                {
                    "Number": user["SmsRecipient"],
                    "Text": user["SmsText"]
                }
            )

        payload = {
            "SenderId": SMS_SENDER_ID,
            "MessageParameters": user_messages,
            "ApiKey": SMS_API_KEY,
            "ClientId": SMS_CLIENT_ID
        }
        return payload

    def send_sms(self, messages=None):
        if messages is None:
            messages = self.get_payload()
        else:
            messages = messages
        payload = dumps(messages)
        headers = {
            'Content-Type': 'application/json',
            'AccessKey': SMS_ACCESS_KEY,
            'Cookie': SMS_COOKIE
        }

        response = requests.request("POST", SMS_URL, headers=headers, data=payload)

        logger.info(response.text.encode('utf8'))

    def save_sms(self, session, messages=None, sent=False):

        if messages is None:
            sms_objects = self.users
        else:
            sms_objects = messages
        for sms_object in sms_objects:
            new_sms = OutgoingSms(
                sender_id='PG_BISON',
                receiver=sms_object["SmsRecipient"],
                message=sms_object["SmsText"],
                sent=sent
            )
            session.add(new_sms)
        session.commit()
