import asyncio
import logging
import os

# from queue_publisher import send_email_to_queue
from .sms import SMS
from sqlalchemy.orm.session import sessionmaker
import xml.etree.ElementTree as ET
from time import sleep

from bison.db import MailResult, DATABASE, init_db
from bison import QUEUE_MESSAGES
from bison.mail import PGBison

LOG_FORMAT = '%(asctime)s %(levelname)s [%(name)s:%(lineno)s] %(module)s %(message)s'
logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
logger = logging.getLogger(__name__)


def parse_xml(mail_string):
    xml_string = mail_string
    xml_string.replace("\n", "").replace("\t", "")
    # constructing an xml tree from xml string
    try:
        tree = ET.ElementTree(ET.fromstring(xml_string))
    except ET.ParseError as error:
        return {"error": error.msg}

    root = tree.getroot()

    sms_records = []
    for record in root.getchildren():
        obj = {}
        for child in record:
            obj[child.tag] = child.text
        sms_records.append(obj)

    return sms_records


def save_emails(session, emails: list):
    for email in emails:
        new_mail = MailResult(uid=email[1], sender=email[2], body=email[0])
        session.add(new_mail)
        session.commit()


def main():
    # initialize database
    init_db()

    # database session
    Session = sessionmaker(DATABASE)
    session = Session()

    # creating email client instance
    bison = PGBison()
    last_mail = session.query(MailResult).order_by(MailResult.id.desc()).first()
    if last_mail:
        uid = last_mail.uid
    else:
        uid = 1

    logger.info(f"restarting with uid: {uid}")
    logger.info("*waiting new mails")
    # wait for new emails, read and save them
    connection = bison.connect()
    while True:
        mail_response = bison.get_mail(connection, str(uid))
        if len(mail_response) > 0:
            logger.info("NEW MAIL RECEIVED...")
            # save emails:
            save_emails(session, mail_response)

            for obj in mail_response:
                logger.info(f"\n\nparsing obj: {obj[0]}")
                sms_objects = parse_xml(str(obj[0]))

                # when there is 'error' in sms_objects, means the xml was not processed successfully, 
                # probably the email was not in the correct format
                if 'error' not in sms_objects:
                    logger.info("sms objects to send sms to...")
                    logger.info(sms_objects)
                    sms = SMS(sms_objects)

                    # try:
                    #     # send message to outgoing mails queue
                    #     await send_email_to_queue(json.dumps({"sender": obj[2], "sms_objects": sms_objects}))
                    # except Exception as e:
                    #     logger.exception(f"ERROR SENDING TO EMAIL QUEUE: {str(e)}")

                    try:
                        # save sms
                        sms.save_sms(session)
                    except Exception as e:
                        logger.exception(f"ERROR SAVING SMS RECORDS: {str(e)}")

                    # check if messages should be queued or not.
                    if not QUEUE_MESSAGES:
                        # send sms to sms objects found:
                        sms.send_sms()

                else:
                    logger.warning(f"error parsing obj..")
                    logger.warning(f"sms_objs_dump: {sms_objects}")
            last_mail = session.query(MailResult).order_by(MailResult.id.desc()).first()
            uid = last_mail.uid
            logger.info(f"new uid: {uid}")
        sleep(1)


def start():
    main()


if __name__ == '__main__':
    start()
